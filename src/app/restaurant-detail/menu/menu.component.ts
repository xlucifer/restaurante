import { menuItem } from '../menu-item/menu-item.model';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { RestaurantsService } from './../../restaurants/restaurants.service';

import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'mt-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {
	
	menu: Observable<menuItem[]>

  	constructor(
		  private restaurantService: RestaurantsService,
		  private route: ActivatedRoute
	  ) { }

  	ngOnInit() {
		this.menu = this.restaurantService
		.menuOfRestaurant(this.route.parent.snapshot.params['id'])
  	}

	addMenuItem( item: menuItem ){
		console.log( item )
	}

}
