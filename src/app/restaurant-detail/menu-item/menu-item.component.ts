import { menuItem } from './menu-item.model';
import { 
  	Component, 
  	OnInit,
	Input,
	Output,
	EventEmitter } from '@angular/core';


@Component({
  selector: 'mt-menu-item',
  templateUrl: './menu-item.component.html'
})

export class MenuItemComponent implements OnInit {
	
	@Input() menuItem: menuItem
	@Output() add = new EventEmitter()

	constructor() { }

  	ngOnInit() {
  	}

	emitAddEvent(){
		this.add.emit(this.menuItem)
	}

}
