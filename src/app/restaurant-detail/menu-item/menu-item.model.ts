export interface menuItem {
    id:string
    name:string
    description:string
    price: number
    imagePath: string
}