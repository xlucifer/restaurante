import { menuItem } from './../menu-item/menu-item.model';
import { CartItem } from './cart-item.model';

export class ShoppingCartService {
    itens: CartItem[] = []
   
    clear(){
        this.itens = []
    }

    add( item: menuItem ){
        let foundItem = this.itens.find((mItem) => mItem.menu.id === item.id )

        if( foundItem ){
            foundItem.quantidade +=  1
        }else{
            this.itens.push(new CartItem(item))
        }
    }

    remove( item: CartItem ){
        this.itens.splice( this.itens.indexOf( item ), 1 )
    }

    total() :number {
        return this.itens
        .map(v => v.valor())
        .reduce(( prev, curr ) => prev + curr, 0)
    }
}