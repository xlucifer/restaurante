import { menuItem } from './../menu-item/menu-item.model';

export class CartItem {
    constructor( public menu: menuItem, public quantidade = 1 ){}
    
   valor(){
        return this.menu.price * this.quantidade
    }

    
}