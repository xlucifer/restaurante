import { ShoppingCartService } from './shopping-cart.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mt-shopping-cart',
  templateUrl: './shopping-cart.component.html'
})
export class ShoppingCartComponent implements OnInit {

  constructor(private sc: ShoppingCartService) { }

  ngOnInit() {
  }

  itens() : any[] {
    return this.sc.itens
  }

  total() :number {
    return this.sc.total()
  }

  addItem( item : any ){
    this.sc.add( item )
  }

  clear( item: any ){
    this.sc.clear()
  }

  remove( item: any ){
    return this.sc.remove( item )
  }

}
