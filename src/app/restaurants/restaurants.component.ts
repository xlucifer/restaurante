import { RestaurantsService } from './restaurants.service';
import { Restaurant } from './restaurant/restaurant.model';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html'
})
export class RestaurantsComponent implements OnInit {
  
  restaurantes: Restaurant[]

  constructor(private restaurantsService: RestaurantsService) { }

  ngOnInit() {
	
	this.restaurantsService.restaurants()
		.subscribe( restaurant => this.restaurantes = restaurant )

  }

}
